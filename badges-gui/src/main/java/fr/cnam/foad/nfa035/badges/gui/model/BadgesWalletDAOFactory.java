package fr.cnam.foad.nfa035.badges.gui.model;

import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import org.apache.logging.log4j.core.config.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component("badgesWalletDAO")
    @Order(value = 1)
    public class BadgesWalletDAOFactory extends AbstractFactoryBean<DirectAcessBadgeWalletDAO> {
        private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";
        @Override
        public Class<?> getObjectType() {
            return DirectAccessBadgeWalletDAOImpl.class;
        }
        @Override
        protected DirectAccessBadgeWalletDAO createInstance() throws IOException {
            return new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        }
    /**
     * accéder au contexte d'injection
     */
    private void createUIComponents() {
        try {
            this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

            badgesWalletController.delegateUIComponentsCreation(this);
            badgesWalletController.delegateUIManagedFieldsCreation(this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}


